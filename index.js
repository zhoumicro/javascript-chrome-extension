//Brian Zhou's hobby app

let myLeads = []
let oldLeads = []
const inputEl = document.getElementById("input-el")
const inputBtn = document.getElementById("input-btn")
const deleteBtn = document.getElementById("delete-btn")
const ulEl = document.getElementById("ul-el")
const leadsFromLocalStorage = JSON.parse(localStorage.getItem("myLeads"))
const tabBtn = document.getElementById("tab-btn")
//console.log(leadsFromLocalStorage)

/*localStorage.setItem("myLeads", "https://www.zhoumicro.co.za/")
leadsName = localStorage.getItem("myLeads")
console.log(leadsName)
localStorage.clear()*/

// 1. Turn the myLeads string into an array
//myLeads = JSON.parse(myLeads)

// 2. Push a new value to the array
//myLeads.push("www.zhou-micro.co.za")

// 3. Turn the array into a string again
//myLeads = JSON.stringify(myLeads)

// 4. Console.log the string using typeof to verify that it's a string
//console.log(typeof myLeads)


let isTrueOrFalse = Boolean(leadsFromLocalStorage)
if (isTrueOrFalse == true){
    myLeads = leadsFromLocalStorage
    render(myLeads)
}

/*const tabs = [
    {url: "https://www.zhoumicro.co.za/"}
]*/

tabBtn.addEventListener("click", function(){

    chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
        console.log(tabs)
        myLeads.push(tabs[0].url)
        localStorage.setItem("myLeads", JSON.stringify(myLeads) )
        render(myLeads)
    })
})

function render(leads) {
    let listItems = ""
    for (let i = 0; i < leads.length; i++) {
        listItems += `
            <li>
                <a target='_blank' href='${leads[i]}'>
                    ${leads[i]}
                </a>
            </li>
        `
    }
    ulEl.innerHTML = listItems  
}

deleteBtn.addEventListener("click", function(){
    localStorage.clear()
    myLeads = []
    render(myLeads)

})

inputBtn.addEventListener("click", function() {
    myLeads.push(inputEl.value)
    inputEl.value = ""
    localStorage.setItem("myLeads", JSON.stringify(myLeads))
    render(myLeads)

    //to verify that it works
    console.log(localStorage.getItem("myLeads"))
})

//function that adds numbers
function add(x, y){
return x + y
}
console.log(add(3, 4))

//functions with arguments practise
function getFirst(arr){
   return myLeads[2]
}
console.log(getFirst(myLeads))